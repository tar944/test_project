function sortArrayImperative(array) {
    array.sort(function(a, b) {
        if (a < b) {
          return -1;
        }
        if (a > b) {
          return 1;
        }
        return 0;
      });

    return array;
}

function sortArrayDeclarative(array) {
    return array.sort((a, b) => a - b);
}


console.log(sortArrayDeclarative([2,8,7,10]))

console.log(sortArrayImperative([2,8,7,10]))
